#include <iostream>
#include "ServerRaspberry.hpp"
#include "Message.hpp"
#include "configServer.h"

using namespace std;

int main()
{
    //system((std::string("./module_stream_photo.sh") + " 0 & 2> /dev/null 1>&2").c_str());
    ServerRaspberry server_rover(PORT);
    server_rover.listenTo(); 

    return 0;
}
