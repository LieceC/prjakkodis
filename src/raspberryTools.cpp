#include "raspberryTools.hpp"

const int posCamOriServ1 = 300; const int posCamOriServ2 = 210;

unsigned int majPosCamera(unsigned int &pos1, unsigned int &pos2)
{
    static PiPCA9685::PCA9685 pca{};
    pca.set_pwm_freq(50.0);
    //std::cout<<pos1<<std::endl;
    //std::cout<<pos2<<std::endl;
    pos1 = (pos1>440)?440:pos1;
    pos1 = (pos1<160)?160:pos1;
    pos2 = (pos2>400)?400:pos2;
    pos2 = (pos2<210)?210:pos2;
    pca.set_pwm(1,0,pos1);
    pca.set_pwm(2,0,pos2);
    usleep(100000);
    return 0;
}

void camera(unsigned int &n) {
    static unsigned int posCamServ1 = posCamOriServ1;
        static unsigned int posCamServ2 = posCamOriServ2;
    for(;;){
        if(checkKthBit(n,8)) {
            posCamServ1 = posCamOriServ1;
            posCamServ2 = posCamOriServ2;
            majPosCamera(posCamServ1,posCamServ2);
        }

        for (int k = 4; k < 8; ++k){
            if(checkKthBit(n,k)){
                switch(k) {
                    case 4 :
                        posCamServ2 += 10;
                        break;
                    case 5 :
                        posCamServ1 -= 10;
                        break;
                    case 6 :
                        posCamServ2 -= 10;
                        break;
                    case 7 :
                        posCamServ1 += 10;
                        break;
                }
            }
        }
        majPosCamera(posCamServ1, posCamServ2);
    }
}

void photo() {
    system("pwd");
    system((std::string("./module_stream_photo.sh") + " 1 & 2> /dev/null 1>&2").c_str());
    usleep(1500000);
    system((std::string("./module_stream_photo.sh") + " 0 & 2> /dev/null 1>&2").c_str());
}

void chenille(int serial_port, unsigned int n) {
    int n1 = n & 15;
    std::string message = std::to_string(n1);
    serialPuts(serial_port, message.c_str());
}

