#include "binTools.hpp"

unsigned int checkKthBit(unsigned int n, unsigned int k)
{
	return ((n & (1 << k)) >> k);
}

unsigned int setKthBit(unsigned int n, unsigned int k)
{
	return (n |= 1 << k);
}

unsigned int unsetKthBit(unsigned int n, unsigned int k)
{
    return (n &= ~(1 << k));
}

unsigned int toogleKthBit(unsigned int n, unsigned int k)
{
	return (n ^= 1 << k);
}

