#include "ServerPilotage.hpp"

ServerPilotage::ServerPilotage(int port):Server(port){
}

ServerPilotage::~ServerPilotage(){
}


void ServerPilotage::dialog(int cfd)
{
    for (;;){
        Message* received = new Message("0","");
        bool filled = this->receiveMessage(cfd,received);

        if (filled){
            json mission = interpretMission(received);
            cout << endl << "Message recu : " << received->message2json() << endl;
        } else {
            delete received;
            break;
        }

        this->sendResponse();

        memset(this->data, 0, ARRAY_SIZE );
        delete received;
    }
}

json ServerPilotage::interpretMission(Message *mes)
{
    // Fonctions d'interpretation des missions
    json mission;
    json ids;
    cout << endl << "Message recu : " << mes->getJ().dump() << endl;
    cout << endl << "ready to parse" << endl;
    mission = mes->getJ();

    cout << endl << "parsing done" << endl;


    std::ofstream htmlMission("tmp/htmlMission.js", ios::out | ios::trunc);

    if(!htmlMission){
        cerr << "Erreur à l'ouverture !" << endl;
        return mission;
    }
    htmlMission << "document.write(\"<ul>\");" << endl;
    string to_write = "";
    if (mission["Corps"].contains("missionType")){
        to_write = "document.write(\"    <li> Type de mission : " + (string)mission["Corps"]["missionType"] + "\");" ;
    htmlMission << to_write << endl;
    } 
    if (mission["Corps"].contains("missionTitle")){
        to_write = "document.write(\"    <li> Intitulé de la mission : " + (string)mission["Corps"]["missionTitle"] + "\");";
        htmlMission << to_write << endl;
    } 
    if (mission["Corps"].contains("missionContent")){
        to_write = "document.write(\"    <li> Détails : " + (string)mission["Corps"]["missionContent"] + "\");";
        htmlMission << to_write << endl;
    } 
    if (mission["Corps"].contains("missionCurrentState")){
        to_write = "document.write(\"    <li> Etat : " + (string)mission["Corps"]["missionCurrentState"] + "\");";
        htmlMission << to_write << endl;
    } 
    if (mission["Corps"].contains("missionLocation")){
        to_write = "document.write(\"    <li> Lieu : " + (string)mission["Corps"]["missionLocation"] + "\");";
        htmlMission << to_write << endl;
    } 
    if (mission["Corps"].contains("missionStartDate")){
        to_write = "document.write(\"    <li> Début : " + (string)mission["Corps"]["missionStartDate"] + "\");";
        htmlMission << to_write << endl;
    } 
    htmlMission << "document.write(\"</ul>\");" << endl;
    htmlMission.close();

    ids["Type"] = 98;
    ids["Corps"]["idMission"] = mission["Corps"]["idMission"] ;
    ids["Corps"]["idPersonnage"] = mission["Corps"]["idPersonnage"] ;
    ids["Corps"]["idRover"] = mission["Corps"]["idRover"] ;
    ids["Corps"]["idPC"] = mission["Corps"]["idPC"] ;

    //Envoi d'une requete au rover de type 98
    Message *mesIds = new Message(ids);

    //cout << endl << "Message envoyé : " << mesIds->message2json() << endl;
    Client client_rover(PORT_CLI,IP_ADDRESS);
    client_rover.connectTo(mesIds);

    delete mesIds;
    return mission;
}

