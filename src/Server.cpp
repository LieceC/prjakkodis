#include "Server.hpp"

Server::Server(int port)
{
    this->data = (char *) malloc(ARRAY_SIZE);
    memset(this->data, 0, ARRAY_SIZE );
    this->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        std::cout << "socket creation failed..." << std::endl;
        exit(0);
    }
    else
        std::cout << "Socket successfully created.." << std::endl;
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        std::cout << "socket bind failed..." << std::endl;
        exit(0);
    }
    else
        std::cout <<"Socket successfully binded..\n" << std::endl;

    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0) {
        std::cout << "Listen failed...\n" << std::endl;
        exit(0);
    }
    else
        std::cout << "Server listening..\n" << std::endl;
}

Server::~Server()
{
    close(this->sockfd);
    free(this->data);
}

void Server::listenTo()
{
    struct sockaddr_in cli;
    unsigned int len;
    len = sizeof(cli);

    //Boucle infini pour que le server attende un client
    for (;;){
        connfd = accept(sockfd, (SA*)&cli, &len);
        if (connfd < 0) {
            std::cout << "server accept failed...\n" << std::endl;
        } else {
            std::cout << "server accept the client...\n" << std::endl;
            dialog(connfd);
            close(connfd);
            char *ipstr = new char[16];
    	    strcpy(ipstr,inet_ntoa(cli.sin_addr));
        }
        std::cout << "Server listening..\n" << std::endl;
    }
}

void Server::dialog(int cfd)
{
    for (;;){
        Message* received = new Message("0","");
        bool filled = this->receiveMessage(cfd,received);

        // In this parent class, nothing is done with the message received
        if (filled){
            cout << endl << "This part need to be implemented in children class" << endl;
            // Implementation of children class
            cout << endl << "Message recu : " << received->message2json() << endl;
        } else {
            delete received;
            break;
        }
        this->sendResponse();
        memset(this->data, 0, ARRAY_SIZE );
        delete received;
    }
}

bool Server::receiveMessage(int cfd, Message *received)
{
    int recu=recv(cfd,this->data,ARRAY_SIZE,0);
    if (recu<=0) {
        return false;
    }
    cout << endl << "Message is received" << endl ;
    received->deserialize(this->data,strlen(this->data));
    return true;
}

void Server::sendResponse()
{
    char copythat[] = "copythat";
    send(this->connfd,copythat,ARRAY_SIZE,0);
}

