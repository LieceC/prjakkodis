#include "Client.hpp"

Client::Client(int port,char* ip_adress)
{
    this->data = (char *) malloc(ARRAY_SIZE);
    memset(this->data, 0, ARRAY_SIZE);
    this->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        std::cout << "Socket creation failed .. " << std::endl;
        exit(0);
    } else {
        std::cout << "Socket succesfully created .." << std::endl;
    }
    bzero(&this->servaddr, sizeof(this->servaddr));

    this->servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip_adress);
    servaddr.sin_port = htons(port);
}

Client::~Client()
{
    free(this->data);
}


void Client::connectTo(Message* tosend)
{
       if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr))
            != 0) {
        std::cout << "connection with the server failed...\n" << std::endl;
        exit(0);
        }
        else
            printf("connected to the server..\n");

        sendMessage(tosend);
}

void Client::sendMessage(Message* mes)
{
    int len = mes->serialize(this->data);
    send(this->sockfd, this->data, ARRAY_SIZE, 0);
}


bool Client::receiveMessage()
{
    char copythat[ARRAY_SIZE];
    int recu=recv(this->sockfd,copythat,ARRAY_SIZE,0);
    if (recu<=0) {
        return false;
    }
    return true;
}

