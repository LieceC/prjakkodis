#include "captationInput.hpp"

void keyboardInput(Message* mes,SDL_Event &Event)
{
    static unsigned int n = 0;
    while (SDL_PollEvent(&Event)) {
        if (Event.type == SDL_QUIT) [[unlikely]] {
            SDL_Quit();
            exit(0);
        }
        if (Event.type == SDL_KEYDOWN){
            if (Event.key.keysym.sym == SDLK_ESCAPE){
                SDL_Quit();
                exit(0);
            }
            if (Event.key.keysym.sym == SDLK_m){
                n = setKthBit(n,10);
            }
            if (Event.key.keysym.sym == SDLK_p){
                n = setKthBit(n,9);
            }
            if (Event.key.keysym.sym == SDLK_o){
                n = setKthBit(n,8);
            }
            if ((Event.key.keysym.sym == SDLK_z) or (Event.key.keysym.sym == SDLK_UP)){
                n = setKthBit(n,0);
            }
            if ((Event.key.keysym.sym == SDLK_q) or (Event.key.keysym.sym == SDLK_LEFT)){
                n = setKthBit(n,3);
            }
            if ((Event.key.keysym.sym == SDLK_s) or (Event.key.keysym.sym == SDLK_DOWN)){
                n = setKthBit(n,2);
            }
            if ((Event.key.keysym.sym == SDLK_d) or (Event.key.keysym.sym == SDLK_RIGHT)){
                n = setKthBit(n,1);
            }
            if (Event.key.keysym.sym == SDLK_i){
                n = setKthBit(n,4);
            }
            if (Event.key.keysym.sym == SDLK_j){
                n = setKthBit(n,7);
            }
            if (Event.key.keysym.sym == SDLK_k){
                n = setKthBit(n,6);
            }
            if (Event.key.keysym.sym == SDLK_l){
                n = setKthBit(n,5);
            }
            if (Event.key.keysym.sym == SDLK_KP_SPACE){
                n = 0 ;
            }
        }
        if (Event.type == SDL_KEYUP){
            if (Event.key.keysym.sym == SDLK_ESCAPE){
                SDL_Quit();
                exit(0);
            }
            if (Event.key.keysym.sym == SDLK_m){
                n = unsetKthBit(n,10);
            }
            if (Event.key.keysym.sym == SDLK_p){
                n = unsetKthBit(n,9);
            }
            if (Event.key.keysym.sym == SDLK_o){
                n = unsetKthBit(n,8);
            }
            if ((Event.key.keysym.sym == SDLK_z) or (Event.key.keysym.sym == SDLK_UP)){
                n = unsetKthBit(n,0);
            }
            if ((Event.key.keysym.sym == SDLK_q) or (Event.key.keysym.sym == SDLK_LEFT)){
                n = unsetKthBit(n,3);
            }
            if ((Event.key.keysym.sym == SDLK_s) or (Event.key.keysym.sym == SDLK_DOWN)){
                n = unsetKthBit(n,2);
            }
            if ((Event.key.keysym.sym == SDLK_d) or (Event.key.keysym.sym == SDLK_RIGHT)){
                n = unsetKthBit(n,1);
            }
            if (Event.key.keysym.sym == SDLK_i){
                n = unsetKthBit(n,4);
            }
            if (Event.key.keysym.sym == SDLK_j){
                n = unsetKthBit(n,7);
            }
            if (Event.key.keysym.sym == SDLK_k){
                n = unsetKthBit(n,6);
            }
            if (Event.key.keysym.sym == SDLK_l){
                n = unsetKthBit(n,5);
            }
        }
    }
    mes->setCorps(to_string(n));
}

