#include "Message.hpp"

Message::Message(string typeOrdre, string corps)
{
    this->j["Type"] = typeOrdre;
    this->j["Corps"] = corps;
//    cout << endl << "Creation d'un message" << endl;
}

Message::Message(json jd){
    this->j = jd;
}

Message::~Message()
{
//    cout << endl << "Destruction d'un message" << endl;
}

string Message::message2json(void){
    return j.dump();
}


int Message::serialize(char * data) {
    std::string s = this->j.dump();
    strcpy(data,s.data());
    return s.length();
}

void Message::deserialize(char * data, int len) {
    string received(data);
    this->j = json::parse(received);
}

string Message::getCorps(){
    return this->j["Corps"];
}

void Message::setCorps(string corps){
    this->j["Corps"] = corps;
}

string Message::getType(){
    return this->j["Type"];
}

void Message::setType(string type){
    this->j["Type"] = type;
}

json Message::getJ(){
    return this->j;
}

void Message::setJ(json j){
    this->j = j;
}

