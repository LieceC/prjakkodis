#include "ServerRaspberry.hpp"

ServerRaspberry::ServerRaspberry(int port):Server(port){
    this->n = 0;
    this->t = new std::thread(camera, std::ref(this->n));
    this->idMission = 0;
    this->idPersonnage = 0;
    this->idRover = 0;
    this->idPC = 0;
    if ((this->serial_port = serialOpen("/dev/ttyAMA0", 9600)) < 0) {
        std::cerr << "Unable to open serial port. Ensure that you are running this code with sudo.\n";
        exit(1);
    }
}

ServerRaspberry::~ServerRaspberry(){
}

void ServerRaspberry::dialog(int cfd)
{
    for (;;){
        Message* received = new Message("0","");
        bool filled = this->receiveMessage(cfd,received);

        if (filled){
            interpretInput(received);
            cout << endl << "Message recu : " << received->message2json() << endl;
        } else {
            delete received;
            break;
        }

        this->sendResponse();

        memset(this->data, 0, ARRAY_SIZE );
        delete received;
    }
}

void ServerRaspberry::interpretInput(Message *mes)
{
    if (stoi(mes->getType())==99){
        this->n = stoi(mes->getCorps());
        std::cout << "n : " << this->n << std::endl;
        chenille(this->serial_port,this->n);
        if (checkKthBit(this->n,9)){
            photo();
            sendLogPhoto();
        }
        if(checkKthBit(this->n,10))
            sendLogMesure();
        return;
    }
    else if (stoi(mes->getType())==98){
        cout << endl << "Mise a jour des identifiants mission" << endl ;
        this->idMission = mes->getJ()["Corps"]["idMission"];
        cout << "nouvelle mission " << this->idMission << endl;
        this->idPersonnage = stoi((string)mes->getJ()["Corps"]["idPersonnage"]);
        cout << "nouveau pers " << this->idPersonnage << endl;
        this->idRover = stoi((string)mes->getJ()["Corps"]["idRover"]);
        cout << "nouveau rover " << this->idRover << endl;
        this->idPC = stoi((string)mes->getJ()["Corps"]["idPC"]);
        cout << "nouveau PC " << this->idPC << endl;
        return;
    }
    return;
}

void ServerRaspberry::sendLogMesure(){
    json j;
    const auto now = std::chrono::system_clock::now();
    std::string date_now = "24-07-2023";
    std::string hour = "16:30:00";
    j["type"] = "mesure";
    j["idMission"] = this->idMission;
    j["sender"] = "client demineur gr4";
    j["idPersonnage"] = this->idPersonnage;
    j["idRover"] = this->idRover;
    j["dateHour"] = date_now + " " + hour;
    j["file"]["name"] = "mesure.json";
    j["file"]["content"]["unite"] = "temperature";
    j["file"]["content"]["valeur"] = "24.56";
    Message *mes = new Message(j);
    cout << endl << "Message envoye : " << mes->message2json() << endl;
    Client client_rover(PORT_CLI,IP_ADDRESS);
    client_rover.connectTo(mes);
}


void ServerRaspberry::sendLogPhoto(){
    json j;
    const auto now = std::chrono::system_clock::now();
    std::string date_now = "24-07-2023";
    std::string hour = "16:30:00";
    j["type"] = "photo";
    j["idMission"] = this->idMission;
    j["sender"] = "client demineur gr4";
    j["idPersonnage"] = this->idPersonnage;
    j["idRover"] = this->idRover;
    j["dateHour"] = date_now + " " + hour;
    // Recuperation de la photo
    j["file"]["name"] = date_now + "-" + hour + ".jpg";
    // encode la photo ./tmp/tmp.jpg
    ifstream fin("./tmp/tmp.jpg", ios::binary);
    if (!fin){
        std::cout << "Image introuvable" << std::endl;
    }
    stringstream buffer;
    buffer << fin.rdbuf();
    std::string img64 = base64_encode(buffer.str(),false);
    j["file"]["content"] = img64;
    Message *mes = new Message(j);
    //cout << endl << "Message envoyé : " << mes->message2json() << endl;
    //cout << endl << "Longueur du message : " << mes->message2json().length() << endl;
    Client client_rover(PORT_CLI,IP_ADDRESS);
    client_rover.connectTo(mes);
}

