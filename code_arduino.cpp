#include <Servo.h>
#include <stdlib.h>

#define MOTG 9
#define MOTD 10
#define STOP 1450
#define AV 1900
#define AR 1000
#define DEMI 450

Servo servoG;
Servo servoD;

unsigned int checkKthBit(unsigned int n, unsigned int k){
  return ((n & (1 << k)) >> k );
}

void decode(int n, int &vRg, int &vRd){
  if (checkKthBit(n,0)){
    vRg -= DEMI;
    vRd += DEMI;
  }
  if (checkKthBit(n,1)){
    vRg -= -.5*DEMI;
    vRd += .5*DEMI;
  }
  if (checkKthBit(n,2)){
    vRg -= -1*DEMI;
    vRd += -1*DEMI;
  }
  if (checkKthBit(n,3)){
    vRg -= .5*DEMI;
    vRd += -.5*DEMI;
  }
}

void setup() {
  Serial.begin(9600);
  servoG.attach(MOTG);
  servoD.attach(MOTD);
  servoG.writeMicroseconds(STOP);
  servoD.writeMicroseconds(STOP);
  Serial.println("Servomoteur mis à l'arret");

}

void loop() {
  if (Serial.available() > 0) {
    String input = Serial.readStringUntil('\n');
    unsigned int n = input.toInt();
    Serial.print("number received : ");
    Serial.println(n);
    int vRg = 0;
    int vRd = 0;
    decode(n,vRg,vRd);
    //int offset = (abs(vRg)>abs(vRd))?abs(vRg):abs(vRd);
    int offset = 1;
    //servoG.writeMicroseconds(STOP+vRg*DEMI/offset);
    //servoD.writeMicroseconds(STOP+vRd*DEMI/offset);
    servoG.writeMicroseconds(STOP+vRg);
    servoD.writeMicroseconds(STOP+vRd);
  }
}
