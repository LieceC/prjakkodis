#include <iostream>
#include "Client.hpp"
#include "Message.hpp"
#include "configClient.h"
#include "captationInput.hpp" 

using namespace std;

int main()
{

    Window AppWindow;
    SDL_Event Event;
    // start inf loop
    json jsend;
    jsend["Type"]=99;
    jsend["Corps"]="";
    Message* tosend = new Message(jsend);
    Message* lastsend = new Message(jsend);
    for(;;){
        keyboardInput(tosend,Event);
        if (lastsend->getCorps()!=tosend->getCorps()){
            std::cout << std::endl << "Message envoyé : " << tosend->message2json() << std::endl;
            lastsend->setCorps(tosend->getCorps());

            Client client_rover(PORT);
            client_rover.connectTo(tosend);
        }
    }

    return 0;
}
