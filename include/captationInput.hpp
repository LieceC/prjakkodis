#ifndef CAPTATIONINPUT_H
#define CAPTATIONINPUT_H

#include "binTools.hpp"
#include "window.h"
#include "Message.hpp"

/**
    Function to read any event from the keyboard (key down and key up event) and stored it in a Message object
    @param Message* A pointer to the message where the input is stored
    @param SDL_Event& A union that contains structures for the different event types (See SDL2 documentation)
**/
void keyboardInput(Message*,SDL_Event&);

#endif // CAPTATIONINPUT_H
