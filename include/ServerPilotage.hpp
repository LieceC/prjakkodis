#ifndef SERVERPILOTAGE_H
#define SERVERPILOTAGE_H
#include "Server.hpp"
#include "Client.hpp"
#include <fstream>

/**
    @class ServerPilotage ServerPilotage.hpp "ServerPilotage.hpp"
**/
class ServerPilotage : public Server {
    public:
        /**
            Call Server Class constructor
        **/
        ServerPilotage(int port);
        /**
            Call Server Class destructor
        **/
        virtual ~ServerPilotage();
        /**
            Function that is used to read a mission from a PC client (overriding the dialog function fromServer class)
            @param int the socket number that is used to communicate
        **/
        void dialog(int);

        /**
            Fonction that'll be used if the JSON message refer to a mission
            It'll parse the whole message and create an usable js file that'll
            be displayed in the WEB GUI
        **/
        json interpretMission(Message*);

    protected:

    private:
        json mission;
};

#endif // SERVERPILOTAGE_H
