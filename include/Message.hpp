#ifndef MESSAGE_H
#define MESSAGE_H

#include "json.hpp"
#include <iostream>
#include <string>

using namespace std;
using json = nlohmann::json;

/**
    @class Message Message.hpp "Message.hpp"
**/
class Message
{
    public:
        /**
            Basic Constructor of a message taking a int (enumtype) and a
            string body
            @param int the enumtype number referring to an action
            @param string the body of the future json message
        **/
        Message(string="",string="");
        /**
            Constructor of a message taking a whole json as an argument
            @param json A json object
        **/
        Message(json jd);
        /**
            Deconstructor of the message class
        **/
        virtual ~Message();
        /**
            Function that stringify the json object
            @return string The stringify json object
        **/
        string message2json();
        /**
            Transform the json object and place into a char* object
            to send it on the network
            @param data the data structure that'll contain our json strified
            @return int the size of the data strcuture
        **/
        int serialize(char * data);
        /**
            Take a char* and the size of the data in it and will
            transform it into an interpretable json
            @param data the data received from the network
            @params len the length of the data inside the char*
        **/
        void deserialize(char * data,int len);
        /**
            Return the corps of the json
        **/
        string getCorps();
        /**
            Setter for the corps of the json
        **/
        void setCorps(string);
        /**
            Getter for the enum order of a json
        **/
        string getType();
        /**
            Setter of the enum order inside a json
        **/
        void setType(string);
        json getJ();
        void setJ(json);

    protected:

    private:
        json j;
};

#endif // MESSAGE_H
