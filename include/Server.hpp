#ifndef SERVER_H
#define SERVER_H
#include <arpa/inet.h> // inet_addr()
#include <netdb.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <strings.h> // bzero()
#include <sys/socket.h>
#include <unistd.h> // read(), write(), close()
#include "Message.hpp"
#include <sys/ipc.h>
#include <sys/shm.h>

#define SA struct sockaddr
#define ARRAY_SIZE 1000

/**
    @class Server Server.hpp "Server.hpp"
    @details Server use object from @link Message Message @endlink.
**/
class Server
{
    public:
        /**
            Constructor of the server
            @param port the port that'll be used by the server to listen
            @return an object Server
        **/
        Server(int port);
        /**
            Deconstructor of the server
        **/
        virtual ~Server();
        /**
            Loop that is used to wait for a connection
            if a connection is found, it'll start the dialog
        **/
        void listenTo();
        /**
            Function that is used to read a msg from a client
            @param int the socket number that is used to communicate
        **/
        void dialog(int);
        /**
            Check if the message is valid and retrieve it 
            Also let us deserialize the message into an interpretable JSON
            @param int The socket number that is used to communicate
            @param Message the message object that'll be used to stock the deserialized JSON
        **/
        bool receiveMessage(int,Message*);
        /**
            Function used to confirm the reception of a message to the client
        **/
        void sendResponse();

        /**
            Getter of the data field
            @return return the value contained in data
        **/
        char* getData(void) {return this->data;}

    protected:
        char* data;

    private:
        int sockfd;
        int connfd;
        int len;
        struct sockaddr_in servaddr;
};

#endif // SERVER_H
