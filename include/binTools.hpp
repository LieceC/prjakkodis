#ifndef BINTOOLS_H
#define BINTOOLS_H
/**
    Check if for a binary n, if the value at place k is 1
    @param int The binary n
    @param int the place of the value to check
    @return 1 if the value is 1, else 0
**/
unsigned int checkKthBit(unsigned int, unsigned int);
/**
    Set the bit k of a  binary n to 1
    @param int The binary n
    @param int the place of the value to set
    @return The new n value
**/
unsigned int setKthBit(unsigned int, unsigned int);
/**
    Unset the bit k of a binary n
    @param int The binary n
    @param int the place of the value to check
    @return The new n value
**/
unsigned int unsetKthBit(unsigned int, unsigned int);
/**
    Reverse the value of the bit k of a binary n
    @param int The binary n
    @param int the place of the value to check
    @return The new n value
**/
unsigned int toogleKthBit(unsigned int, unsigned int);

#endif // BINTOOLS_H
