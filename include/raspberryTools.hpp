#ifndef RASPBERRYTOOLS_H
#define RASPBERRYTOOLS_H
#include <PiPCA9685/PCA9685.h>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "binTools.hpp"

/**
    This function take has an input 2 values corresponding to the X and Y of the camera
    and will update the camera is position according to those
    @param unsigned int pos1 Value of position for the first servomotor
    @param unsigned int pos2 Value of position for the second servomotor
    @return 0
**/
unsigned int majPosCamera(unsigned int &, unsigned int &);

/**
    This function is a loop that will turn for the whole program duration in a thread
    It'll check every iteration if the value of the pressed key by the user changed
    and will update the camera mouvement according to it
    @param unsigned int n Integer corresponding to the input of the operator (bit 4 to 7 are used)
**/
void camera(unsigned int &n);

/**
    This function allow us to start a shell script to take a picture and restart the video straeming
    of the IMX519 camera
**/
void photo();

/**
    This function allow us to send information of our binary n through a serial port ID to the arduino program
    @param int serial_port Serial port ID ccorresponding to the Tx-Rx connection
    @param unsigned int n Integer corresponding to the input of the operator (bit 0 to 3 are used)
**/
void chenille(int serial_port, unsigned int n);

#endif // RASPBERRYTOOLS_H

