#ifndef CLIENT_H
#define CLIENT_H
#include <arpa/inet.h> // inet_addr()
#include <netdb.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <strings.h> // bzero()
#include <sys/socket.h>
#include <unistd.h> // read(), write(), close()
#include "Message.hpp"
#include "configClient.h"
#include <sys/ipc.h>
#include <sys/shm.h>

#define SA struct sockaddr
#define ARRAY_SIZE 1000

/**
    @class Client Client.hpp "Client.hpp"
**/
class Client
{
    public:
        /**
            Contructor of a TCP client to communicate with a command center
            @param port Value of the port of the server to connect to
            @param ip_adress the ip_adress in a char* format of the command center
            @return A client class
        **/

        Client(int port, char* ip_adress);
         /**
            Deconstructor of a TCP client
        **/
        virtual ~Client();
         /**
            Function that takes a message to send and try to
            establish the final connection to the client
            @param Message The message that has to be sent
        **/
        void connectTo(Message*);
        /**
            Function that send the message through a TCP socket
            @param Message The message that has to be sent
        **/
        void sendMessage(Message*);
        /**
            Function that receive a message from another TCP pair
            @return A boolean that is true if the message has been properly received
            , false otherwise
        **/
        bool receiveMessage();
        /**
            Function that receive a message from another TCP pair
            @return A boolean that is true if the message has been properly received
            , false otherwise
        **/
        char* getData(void) {return this->data;}

    protected:

    private:
        int sockfd;
        int sonnfd;
        struct sockaddr_in servaddr;
        char * data;
};

#endif // CLIENT_H

