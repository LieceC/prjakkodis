#ifndef SERVERRASPBERRY_H
#define SERVERRASPBERRY_H
#include "Server.hpp"
#include "Client.hpp"
#include <fstream>
#include "binTools.hpp"
#include "raspberryTools.hpp"
#include <thread>
#include "base64.h"

/**
    @class ServerRaspberry ServerRaspberry.hpp "ServerRasberry.hpp"
**/
class ServerRaspberry : public Server {
    public:
        /**
            Call Server Class constructor
        **/
        ServerRaspberry(int port);
        /**
            Call Server Class destructor
        **/
        virtual ~ServerRaspberry();
        /**
            Function that is used to read an input or an ID update of mission from a Pilotage server or client (overriding the dialog function from Server class)
            @param int the socket number that is used to communicate
        **/
        void dialog(int);

        /**
            This function take a message as an input.
            If this message contains key pressed informations by the user (Type 99), it'll interpret the corresponding binary, and sent the appropriate information to the arduino (by a TXRX connection) allowing the user to control the rover, to the PCA9685 card and the camera servomotor (by I2C connection) allowing the user to control the camera or to trigger the sending of a mesure log or a photo log to the PC server.
            If this message contains mission ID informations send by the Pilot server (Type 98), it'll update those informations.
            Else does nothing.
            @param Message* mes Pointer to the message object containing the input or the mission ID
        **/
        void interpretInput(Message*);
        /**
            Function that'll be triggered when the rover has a new mesure to communicate.
            This function create a json containing the mesure information and the mission IDs previously send by command center.
            The picture will be send to the idPC command center.
        **/
        void sendLogMesure();
        /**
            Function that'll be triggered when the user press the key to take a photo.
            This function will stop the video stream in order to take a picture.
            The picture will be send to the idPC command center in a base64 compression
        **/
        void sendLogPhoto();

    protected:

    private:
        int serial_port;
        unsigned int n;
        unsigned int idMission;
        unsigned int idPersonnage;
        unsigned int idRover;
        unsigned int idPC;
        std::thread *t;
};

#endif // SERVERRASPBERRY_H
