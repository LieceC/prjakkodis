# prjAKKODIS

This repository is the final state of our development for the robot project for the POEI AKKODIS - AJC Formation

Authors : Lièce Cherchour - Quentin Desbonnets
Supervision : Steeve Assous - Sylvain Besseron

## Documentation

To generate the documentation

```
cd doc
doxygen config-file 
```

## Compilation

You can use cmake to compile the project, an exemple is given in the repository. Adapt it to your usage.

```
cmake -B build
cmake --build build
# cmake --build build --clean-first
```

