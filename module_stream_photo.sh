#!/bin/bash

case $1 in
    0)
        libcamera-vid --framerate 24 --nopreview --inline -t 0 --width 640 --height 480 --listen -o - | ffmpeg -i - -preset ultrafast -tune zerolatency -vcodec libx264 -r 24 -s 640x480 -f rtsp rtsp://57.128.107.12:8554/mystream 2> /dev/null 1>&2
        ;;
    1)
    mkdir -p tmp
	ps -ef | grep libcamera | head -1 | awk '{print $2}' | xargs kill
#        libcamera-still --nopreview --width 640 --height 480 -t 1000 -o tmp/$(date +%y%m%d_%H%M%S).jpg 2> /dev/null 1>&2
        libcamera-still --nopreview --width 640 --height 480 -t 1000 -o tmp/tmp.jpg 2> /dev/null 1>&2
        ;;
    *)
        echo "Argument non pris en compte : 0 - stream , 1 - photo"
esac

exit 0
